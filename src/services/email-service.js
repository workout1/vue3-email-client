import axios from "axios";

const EMAIL_SERVER_URL = "http://localhost:3000/emails";

export const fetchEmails = () => {
  return axios.get(EMAIL_SERVER_URL);
};

export const updateEmail = email => {
  return axios.put(`${EMAIL_SERVER_URL}/${email.id}`, email);
};

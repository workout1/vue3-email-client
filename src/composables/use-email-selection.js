import { reactive } from "vue";

import { updateEmail } from "@/services/email-service";

let emails = reactive(new Set());

const useEmailSelection = () => {
  const toggle = email => {
    if (emails.has(email)) {
      emails.delete(email);
    } else {
      emails.add(email);
    }
  };

  const clear = () => emails.clear();

  const addMultiple = newEmails => {
    newEmails.forEach(e => emails.add(e));
  };

  const toggleField = fn => {
    emails.forEach(e => {
      fn(e);
      updateEmail(e);
    });
  };

  const markRead = () => {
    toggleField(e => (e.read = true));
  };

  const markUnread = () => {
    toggleField(e => (e.read = false));
  };

  const archive = () => {
    toggleField(e => (e.archived = true));
    clear();
  };

  const unarchive = () => {
    toggleField(e => (e.archived = false));
  };
  return {
    emails,
    toggle,
    clear,
    addMultiple,
    markRead,
    markUnread,
    archive,
    unarchive
  };
};

export default useEmailSelection;
